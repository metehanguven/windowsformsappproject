﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsAppProject.ClientSide;

namespace WindowsFormsAppProject
{
    public class Game
    {
        public static string Username;
        private List<List<Flake>> flakes = new List<List<Flake>>();
        private Size boardSize = new Size();
        private int point = 0;
        private int totalPoint = 0;
        private Point startLocation = new Point();
        private Random rnd = new Random();
        private UniqueRandom uniqueRandom;
        private Flake selectedFlake;
        public int[,] transformation;
        public Process process;
        private Timer timer = new Timer();
        private Timer commandTimer = new Timer();
        private int timer_counter = 0;
        private Label label;
        private Client client;
        private int commandNumber;

        public List<List<Flake>> GetFlakes
        {
            get { return this.flakes; }
        }
        public Point StartLocation
        {
            get { return this.startLocation; }
            set { this.startLocation = value; }
        }
        public int GetTotalPoint
        {
            get { return this.totalPoint; }
        }

        public void ConnectServer(Client client)
        {
            this.client = client;
        }

        private void SendMessageToServer(string message)
        {
            if (this.client != null)
            {
                this.client.SendMessage(message);
            }
        }
        private void LoadDifficulty()
        {
            if (Settings2.Default.rdb1)
            {
                boardSize = new Size(15, 15);
                point = 1;
            }
            else if (Settings2.Default.rdb2)
            {
                boardSize = new Size(9, 9);
                point = 3;
            }
            else if (Settings2.Default.rdb3)
            {
                boardSize = new Size(6, 6);
                point = 5;
            }
            else
            {
                boardSize = new Size(int.Parse(Settings2.Default.CustomText1), int.Parse(Settings2.Default.CustomText2));
                point = 2;
            }

            this.uniqueRandom = new UniqueRandom(0, this.boardSize.Width * this.boardSize.Height);
        }
        private void LoadFlakes()
        {
            for (int i = 0; i < boardSize.Height; i++)
            {
                List<Flake> horizontal = new List<Flake>();
                for (int j = 0; j < boardSize.Width; j++)
                {
                    var flake = new Flake();
                    flake.Name = "Flake" + i.ToString() + "_" + j.ToString();
                    flake.Click += Flake_Click;
                    flake.WorldLocation = new Point(j, i);
                    flake.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    flake.BackColor = Color.DarkTurquoise;
                    flake.BaseColor = flake.BackColor;
                    flake.Size = new Size(600 / boardSize.Width, 600 / boardSize.Height);
                    flake.Location = new Point(this.startLocation.X + j * (600 / boardSize.Width + 5), this.startLocation.Y + i * (600 / boardSize.Height + 5));
                    horizontal.Add(flake);
                }
                this.flakes.Add(horizontal);
            }
        }
        private void Flake_Click(object sender, EventArgs e)
        {
            Flake flake = (Flake)sender;

            if (flake.Conquered)
            {
                if (this.selectedFlake != null && this.selectedFlake.Name != flake.Name)
                {
                    this.selectedFlake.SelectedFlake = false;
                    this.selectedFlake.BackColor = this.selectedFlake.BaseColor;
                    SendMessageToServer($"Release the flake. %l-{selectedFlake.WorldLocation.X}-{selectedFlake.WorldLocation.Y}");
                }

                this.selectedFlake = flake;
                SendMessageToServer($"({selectedFlake.WorldLocation.X + 1}-{selectedFlake.WorldLocation.Y + 1})" +
                    $":[{selectedFlake.BaseColor.ToString()},{selectedFlake.GetShape().ToString()}] flake selected." + Environment.NewLine +
                    $"%s-{selectedFlake.WorldLocation.X}-{selectedFlake.WorldLocation.Y}");

                if (!flake.SelectedFlake)
                {
                    flake.BackColor = ControlPaint.LightLight(flake.BaseColor);
                    flake.SelectedFlake = true;
                }
            }
            else
            {
                if (this.selectedFlake != null)
                {
                    //RUN path algorithm
                    Transform(this.selectedFlake.WorldLocation, flake.WorldLocation);
                    GFG fG = new GFG();
                    bool road = fG.isPath(this.transformation);
                    fG.DetectPath(this.transformation);
                    if (road)
                    {
                        flake.BackColor = Color.Yellow;
                        this.process = fG.process;
                        SendMessageToServer($"({selectedFlake.WorldLocation.X + 1}-{selectedFlake.WorldLocation.Y + 1})" +
                            $":[{selectedFlake.BaseColor.ToString()},{selectedFlake.GetShape().ToString()}] moving to" +
                            $" ({flake.WorldLocation.X + 1}-{flake.WorldLocation.Y + 1})" + Environment.NewLine +
                            $"%m-{flake.WorldLocation.X}-{flake.WorldLocation.Y}|f-{selectedFlake.WorldLocation.X}-{selectedFlake.WorldLocation.Y}");

                        PerformDirectives();
                    }
                    else
                        flake.BackColor = Color.Gray;

                    //for (int i = 0; i < process.Directives.Count; i++)
                    //{
                    //    this.flakes[process.Directives[i].RowIndex][process.Directives[i].ColumnIndex].BackColor = Color.White;
                    //}
                }
            }
        }
        private void CommandTimer_Tick(object sender, EventArgs e)
        {
            var commands = this.client.GetClientsTransmission.GetTransmission;

            if (this.commandNumber != commands.Count && commands.Count > 0
               && commands[commands.Count - 1].IndexOf('%') != -1 && commands[commands.Count - 1].IndexOf('<') == -1)
            {
                var cmd = commands[commands.Count - 1].Split('%');

                if (cmd[0].IndexOf("request") != -1)
                {
                    var kbm = cmd[1].Split('|');
                    var kbm0 = kbm[0].Split('-');
                    var kbm1 = kbm[1].Split('-');
                    Point dest = new Point(int.Parse(kbm0[1]), int.Parse(kbm0[2]));
                    Point source = new Point(int.Parse(kbm1[1]), int.Parse(kbm1[2]));

                    var transform = Transformation(source, dest);
                    GFG fG = new GFG();
                    bool road = fG.isPath(transform);

                    if (road)
                    {
                        SendMessageToServer($"%a-{dest.X}-{dest.Y}");
                        this.flakes[dest.Y][dest.X].BackColor = this.flakes[source.Y][source.X].BackColor;
                        this.flakes[dest.Y][dest.X].SetShape(this.flakes[source.Y][source.X].GetShape());
                        this.flakes[dest.Y][dest.X].BaseColor = this.flakes[source.Y][source.X].BaseColor;
                        this.flakes[dest.Y][dest.X].Conquered = true;
                        this.uniqueRandom.Exchange(this.flakes[source.Y][source.X].WorldLocation.Y * this.boardSize.Width + this.flakes[source.Y][source.X].WorldLocation.X
                            , dest.Y * this.boardSize.Width + dest.X);
                        this.flakes[source.Y][source.X].BackColor = Color.DarkTurquoise;
                        this.flakes[source.Y][source.X].BaseColor = Color.DarkTurquoise;
                        this.flakes[source.Y][source.X].SetShape(Flake.Shapes.Square);
                        this.flakes[source.Y][source.X].Conquered = false;
                    }
                    else
                    {
                        SendMessageToServer($"%n-{dest.X}-{dest.Y}");
                    }
                }
            }
            
            this.commandNumber = commands.Count;


        }
        public string GetOptions()
        {
            string option = "%"; //option key

            //Board size and point
            option += boardSize.Width.ToString() + "," + boardSize.Height.ToString() + "," + point.ToString() + "|";

            //0 triangle 1 circle 2 square
            //Coordinate,shape,color
            string flakeCoordinate = string.Empty;
            for (int i = 0; i < this.flakes.Count; i++)
            {
                for (int j = 0; j < this.flakes[i].Count; j++)
                {
                    if (this.flakes[i][j].Conquered)
                    {
                        flakeCoordinate += i.ToString() + "-" + j.ToString() + "<" + Convert.ToInt32(this.flakes[i][j].GetShape()) +
                            "<" + this.flakes[i][j].BaseColor.ToArgb();
                        option += flakeCoordinate + ",";
                        flakeCoordinate = "";
                    }
                }
            }
            option = option.Remove(option.Length - 1, 1);
            //option += "|";

            return option;
        }


        public Flake SelectRandomFlake()
        {
            //Select shape
            //0 = triangle, 1 = circle, 2=square;
            List<Flake.Shapes> shapes = new List<Flake.Shapes>();

            if (Settings2.Default.chcüç)
                shapes.Add(Flake.Shapes.Triangle);

            if (Settings2.Default.chcyuv)
                shapes.Add(Flake.Shapes.Circle);

            if (Settings2.Default.chckare)
                shapes.Add(Flake.Shapes.Square);

            Flake.Shapes shape = shapes[rnd.Next(0, shapes.Count)];

            //Select color
            //0 = blue, 1 = red, 2 = yellow, 3 = purple, 4= green, 5 = orange
            List<Color> colors = new List<Color>();

            if (Settings2.Default.chcblue)
                colors.Add(Color.DodgerBlue);
            if (Settings2.Default.chcred)
                colors.Add(Color.Red);
            if (Settings2.Default.chcyellow)
                colors.Add(Color.Yellow);
            if (Settings2.Default.chcpurple)
                colors.Add(Color.Purple);
            if (Settings2.Default.chcgreen)
                colors.Add(Color.Green);
            if (Settings2.Default.chcorange)
                colors.Add(Color.Orange);

            Color color = colors[rnd.Next(0, colors.Count)];

            int element = this.uniqueRandom.GenerateUniqueRandom();
            int m = element / this.boardSize.Width;
            int n = element % this.boardSize.Width;

            var flake = this.flakes[m][n];
            flake.BackColor = color;
            flake.BaseColor = color;
            flake.SetShape(shape);
            flake.Conquered = true;

            return flake;
        }
        public void TripleSelectFlake()
        {
            var p1 = SelectRandomFlake();
            var p2 = SelectRandomFlake();
            var p3 = SelectRandomFlake();

            SendMessageToServer($"Created triple flakes. %c|{p1.WorldLocation.X}-{p1.WorldLocation.Y}," +
                $"{Convert.ToInt32(p1.GetShape())},{p1.BaseColor.ToArgb()}|" +
                $"{p2.WorldLocation.X}-{p2.WorldLocation.Y},{Convert.ToInt32(p2.GetShape())},{p2.BaseColor.ToArgb()}|" +
                $"{p3.WorldLocation.X}-{p3.WorldLocation.Y},{Convert.ToInt32(p3.GetShape())},{p3.BaseColor.ToArgb()}");

            Crash();
            label.Text = "Point" + Environment.NewLine + this.totalPoint.ToString();

            if (this.uniqueRandom.Zero)
            {
                MessageBox.Show("Game over!" + Environment.NewLine + "Your point is " + this.totalPoint.ToString());
            }
        }

        public void Transform(Point source, Point destination)
        {
            int[,] matrix = new int[this.flakes.Count, this.flakes[0].Count];

            for (int i = 0; i < this.flakes.Count; i++)
            {
                for (int j = 0; j < this.flakes[0].Count; j++)
                {
                    if (this.flakes[i][j].Conquered)
                        matrix[i, j] = 0;
                    else
                        matrix[i, j] = 3;
                }
            }

            matrix[source.Y, source.X] = 1;
            matrix[destination.Y, destination.X] = 2;

            this.transformation = matrix;
        }

        public int[,] Transformation(Point source, Point destination)
        {
            int[,] matrix = new int[this.flakes.Count, this.flakes[0].Count];

            for (int i = 0; i < this.flakes.Count; i++)
            {
                for (int j = 0; j < this.flakes[0].Count; j++)
                {
                    if (this.flakes[i][j].Conquered)
                        matrix[i, j] = 0;
                    else
                        matrix[i, j] = 3;
                }
            }

            matrix[source.Y, source.X] = 1;
            matrix[destination.Y, destination.X] = 2;

            return matrix;
        }

        public void PerformDirectives()
        {
            timer.Enabled = true;
            timer_counter = 0;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            int col = this.process.Directives[this.timer_counter].ColumnIndex;
            int row = this.process.Directives[this.timer_counter].RowIndex;

            this.flakes[row][col].BackColor = Color.Brown;

            if (timer_counter > 0)
                this.flakes[process.Directives[timer_counter - 1].RowIndex][process.Directives[timer_counter - 1].ColumnIndex]
                    .BackColor = this.flakes[process.Directives[timer_counter - 1].RowIndex][process.Directives[timer_counter - 1].ColumnIndex].BaseColor;

            this.timer_counter++;

            if (timer_counter == this.process.Directives.Count)
            {
                timer.Enabled = false;

                //Hamle sonunda.

                this.flakes[row][col].BackColor = this.selectedFlake.BackColor;
                this.flakes[row][col].SetShape(this.selectedFlake.GetShape());
                this.flakes[row][col].BaseColor = this.selectedFlake.BaseColor;
                this.flakes[row][col].SelectedFlake = true;
                this.flakes[row][col].Conquered = true;
                this.uniqueRandom.Exchange(this.selectedFlake.WorldLocation.Y * this.boardSize.Width + this.selectedFlake.WorldLocation.X
                    , row * this.boardSize.Width + col);
                this.selectedFlake.BackColor = Color.DarkTurquoise;
                this.selectedFlake.BaseColor = Color.DarkTurquoise;
                this.selectedFlake.SetShape(Flake.Shapes.Square);
                this.selectedFlake.SelectedFlake = false;
                this.selectedFlake.Conquered = false;
                this.selectedFlake = this.flakes[row][col];

                TripleSelectFlake();    //3 tane daha belir.
            }
        }

        public void Crash()
        {
            for (int i = 0; i < this.flakes.Count; i++)
            {
                for (int j = 0; j < this.flakes[0].Count; j++)
                {
                    bool flag = Stick_Col(i, j);
                    bool flag2 = Stick_Row(i, j);
                    //Row-control
                    if (flag2)
                    {
                        for (int k = 0; k < 5; k++)
                            RemoveFlake(new Point(j, i + k));
                        this.totalPoint += point;
                    }

                    if (flag)
                    {
                        for (int k = 0; k < 5; k++)
                            RemoveFlake(new Point(j + k, i));
                        this.totalPoint += point;
                    }

                    if (flag || flag2)
                        return;
                }
            }
        }
        private bool Stick_Col(int row, int col)
        {
            //Col-stick control
            if (col + 4 < this.flakes[0].Count)
            {
                for (int i = 1; i < 5; i++)
                {
                    if (this.flakes[row][col].Conquered && this.flakes[row][col + i].Conquered && this.flakes[row][col].BaseColor == this.flakes[row][col + i].BaseColor &&
                        this.flakes[row][col].GetShape() == this.flakes[row][col + i].GetShape()) { }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            else
                return false;
        }
        private bool Stick_Row(int row, int col)
        {
            //Row-stick
            if (row + 4 < this.flakes.Count)
            {
                for (int i = 1; i < 5; i++)
                {
                    if (this.flakes[row][col].Conquered && this.flakes[row + i][col].Conquered && this.flakes[row][col].BaseColor == this.flakes[row + i][col].BaseColor &&
                        this.flakes[row][col].GetShape() == this.flakes[row + i][col].GetShape()) { }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            else
                return false;
        }
        public void RemoveFlake(Point world)
        {
            int global = world.Y * this.boardSize.Width + world.X;

            var f = this.flakes[world.Y][world.X];
            f.Conquered = false;
            f.SelectedFlake = false;
            f.BackColor = Color.DarkTurquoise;
            f.BaseColor = Color.DarkTurquoise;
            f.SetShape(Flake.Shapes.Square);
            if (this.selectedFlake != null && this.selectedFlake.Name == f.Name)
                this.selectedFlake = null;

            if (!this.uniqueRandom.Contain(global))
                this.uniqueRandom.Add(global);
        }

        public void StartGame(Label label)
        {
            timer.Interval = 500;
            timer.Tick += Timer_Tick;
            this.label = label;

            commandTimer.Interval = 100;
            commandTimer.Tick += CommandTimer_Tick;

            if (this.client != null)
                commandTimer.Enabled = true;

            LoadDifficulty();
            LoadFlakes();
            TripleSelectFlake();
        }

        

        public class Process
        {
            public List<DirectiveProcess> Directives = new List<DirectiveProcess>();
            public Point Source { get; set; }
            public Point Destination { get; set; }
        }
        public class DirectiveProcess
        {
            public int RowIndex { get; set; }
            public int ColumnIndex { get; set; }

            public DirectiveProcess(int row, int col)
            {
                this.RowIndex = row;
                this.ColumnIndex = col;
            }
        }
        public class GFG
        {
            public class QItem
            {
                public int Row;
                public int Col;
                public int Dist;
                public int Sq;

                public QItem(int row, int col, int dist, int sq)
                {
                    this.Row = row;
                    this.Col = col;
                    this.Dist = dist;
                    this.Sq = sq;
                }
            }

            public Process process = new Process();

            private List<QItem> list = new List<QItem>();

            public bool isPath(int[,] matrix)
            {
                // Defining visited array to keep
                // track of already visited indexes
                bool[,] visited = new bool[matrix.GetLength(0), matrix.GetLength(1)];

                // Flag to indicate whether the
                // path exists or not
                bool flag = false;

                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {

                        // If matrix[i][j] is source
                        // and it is not visited
                        if (matrix[i, j] == 1 && !visited[i, j])
                            if (isPath(matrix, i, j, visited))
                            {
                                // If path exists
                                flag = true;
                                break;
                            }
                    }
                }
                return flag;
            }

            // Method for checking boundaries
            public bool isSafe(int i, int j, int[,] matrix)
            {
                if (i >= 0 && i < matrix.GetLength(0) &&
                    j >= 0 && j < matrix.GetLength(1))
                    return true;

                return false;
            }

            public bool isPath(int[,] matrix, int i, int j, bool[,] visited)
            {
                if (isSafe(i, j, matrix) && matrix[i, j] != 0 && !visited[i, j])
                {
                    // Make the cell visited
                    visited[i, j] = true;

                    // If the cell is the required
                    // destination then return true
                    if (matrix[i, j] == 2)
                        return true;

                    // Traverse up
                    bool up = isPath(matrix, i - 1, j, visited);

                    // If path is found in up
                    // direction return true
                    if (up)
                    {
                        return true;
                    }

                    // Traverse left
                    bool left = isPath(matrix, i,
                                    j - 1, visited);

                    // If path is found in left
                    // direction return true
                    if (left)
                    {
                        return true;
                    }
                    // Traverse down
                    bool down = isPath(matrix, i + 1,
                                    j, visited);

                    // If path is found in down
                    // direction return true
                    if (down)
                    {
                        return true;
                    }
                    // Traverse right
                    bool right = isPath(matrix, i, j + 1,
                                        visited);

                    // If path is found in right
                    // direction return true
                    if (right)
                    {
                        return true;
                    }
                }

                // No path has been found
                return false;
            }

            public int MinDistance(int[,] matrix)
            {
                QItem source = new QItem(0, 0, 0, -1);
                QItem destination = new QItem(0, 0, 0, 0);

                bool[,] visited = new bool[matrix.GetLength(0), matrix.GetLength(1)];

                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        if (matrix[i, j] == 0)
                            visited[i, j] = true;
                        else
                            visited[i, j] = false;

                        if (matrix[i,j] == 1)
                        {
                            source.Row = i;
                            source.Col = j;
                        }

                        if (matrix[i,j] == 2)
                        {
                            destination.Row = i;
                            destination.Col = j;
                        }
                    }
                }

                Queue<QItem> q = new Queue<QItem>();
                q.Enqueue(source);
                visited[source.Row, source.Col] = true;
                while (q.Count != 0)
                {
                    QItem p = q.Dequeue();
                    list.Add(p);

                    if (matrix[p.Row, p.Col] == 2)
                        return p.Dist;

                    //moving up
                    if (p.Row - 1 >= 0 && visited[p.Row - 1, p.Col] == false)
                    {
                        q.Enqueue(new QItem(p.Row - 1, p.Col, p.Dist + 1, DistanceSq(p.Col, p.Row - 1, destination.Col, destination.Row)));
                        visited[p.Row - 1, p.Col] = true;
                    }

                    //moving down
                    if (p.Row + 1 < matrix.GetLength(0) && visited[p.Row + 1, p.Col] == false)
                    {
                        q.Enqueue(new QItem(p.Row + 1, p.Col, p.Dist + 1, DistanceSq(p.Col, p.Row + 1, destination.Col, destination.Row)));
                        visited[p.Row + 1, p.Col] = true;
                    }

                    //moving right
                    if (p.Col + 1 < matrix.GetLength(1) && visited[p.Row, p.Col + 1] == false)
                    {
                        q.Enqueue(new QItem(p.Row, p.Col + 1, p.Dist + 1, DistanceSq(p.Col + 1, p.Row, destination.Col, destination.Row)));
                        visited[p.Row, p.Col + 1] = true;
                    }

                    //moving left
                    if (p.Col - 1 >= 0 && visited[p.Row, p.Col - 1] == false)
                    {
                        q.Enqueue(new QItem(p.Row, p.Col - 1, p.Dist + 1, DistanceSq(p.Col - 1, p.Row, destination.Col, destination.Row)));
                        visited[p.Row, p.Col - 1] = true;
                    }
                }
                return -1;
            }

            public int DistanceSq(int x1, int y1, int x2, int y2)
            {
                return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
            }

            public void DetectPath(int[,] matrix)
            {
                int depth = MinDistance(matrix);

                int selectedIndex = 0;
                int min = int.MaxValue;

                for (int i = 1; i <= depth; i++)
                {
                    for (int j = 0; j < this.list.Count; j++)
                    {
                        if (list[j].Dist == i && min >= list[j].Sq)
                        {
                            min = list[j].Sq;
                            selectedIndex = j;
                        }
                    }
                    min = int.MaxValue;
                    this.process.Directives.Add(new DirectiveProcess(list[selectedIndex].Row, list[selectedIndex].Col));

                }
            }

        }
        public class UniqueRandom
        {
            Random rnd = new Random();
            int min, max;
            int h_max;
            List<int> numbers = new List<int>();
            public UniqueRandom(int min, int max)
            {
                this.min = min;
                this.max = max;
                this.h_max = max - min;

                for (int i = 0; i < h_max; i++)
                {
                    numbers.Add(min + i);
                }
            }
            public void Exchange(int source, int destination)
            {
                int index = numbers.IndexOf(destination);

                numbers[index] = source;

            }
            public bool Contain(int dest)
            {
                if (numbers.IndexOf(dest) != -1 && numbers.IndexOf(dest) < h_max)
                    return true;
                else
                    return false;
            }
            public void Add(int dest)
            {
                this.h_max++;
                numbers[h_max] = dest;
            }
            public bool Zero
            {
                get 
                { 
                    if (h_max <= 0) return true;
                    else return false;
                }
            }
            public int GenerateUniqueRandom()
            {
                int random = rnd.Next(0, h_max);

                int x = numbers[random];

                numbers[random] = numbers[h_max - 1];
                h_max--;

                return x;
            }
        }
    }
}
