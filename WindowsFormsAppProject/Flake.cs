﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppProject
{
    public class Flake : Button
    {
        private bool conquered;
        private Shapes shape;
        public enum Shapes
        {
            Triangle = 0,
            Circle = 1,
            Square = 2,
        }

        public Flake()
        {

        }
        
        public void SetShape(Shapes shape)
        {
            this.shape = shape;
            switch (shape)
            {
                case Shapes.Triangle:
                    {
                        GraphicsPath gp = new GraphicsPath();
                        Point[] points = new Point[3];
                        points[0] = new Point(0, this.Size.Height);
                        points[1] = new Point(this.Size.Width, this.Size.Height);
                        points[2] = new Point(this.Size.Width / 2, 0);
                        gp.AddPolygon(points);
                        this.Region = new Region(gp);
                        gp.Dispose();
                        break;
                    }
                case Shapes.Circle:
                    {
                        GraphicsPath g = new GraphicsPath();
                        g.AddEllipse(new RectangleF(0, 0, this.Size.Width, this.Size.Height));
                        this.Region = new Region(g);
                        g.Dispose();
                        break;
                    }
                case Shapes.Square:
                    {
                        GraphicsPath g = new GraphicsPath();
                        g.AddRectangle(new RectangleF(0, 0, this.Size.Width, this.Size.Height));
                        this.Region = new Region(g);
                        g.Dispose();
                        break;
                    }
            }
        }
        public Shapes GetShape()
        {
            return this.shape;
        }
        
        public bool Conquered
        {
            get { return this.conquered; }
            set { this.conquered = value; }
        }

        public Point WorldLocation { get; set; }
        public bool SelectedFlake { get; set; }
        public Color BaseColor { get; set; }
    }
}
