﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsAppProject.ClientSide;

namespace WindowsFormsAppProject
{
    public partial class Main : Form
    {
        ClientsTransmission transmission = new ClientsTransmission();
        Client client;
        GameForm game;
        public Main()
        {
            InitializeComponent();
            this.Text = "Connection ID: " + Game.Username.ToUpper();
        }

        private void btnex_Click(object sender, EventArgs e)
        {
            new LogSc().Show();
            this.Close();
        }

        private void btnset_Click(object sender, EventArgs e)
        {
            new Dif().Show();
            this.Hide();
        }

        private void btnpro_Click(object sender, EventArgs e)
        {
            new FormProfile().Show();
            this.Hide();
        }

        private void btnstart_Click(object sender, EventArgs e)
        {
            
            if (radio_single.Checked)
            {
                game = new GameForm();
                game.Location = new Point(145, 10);
                this.Controls.Add(game);
            }
            else
            {
                client.SendMessage(this.game.GetOptions);
            }
        }

        private void radio_multiplayer_CheckedChanged(object sender, EventArgs e)
        {
            connectgame_btn.Visible = radio_multiplayer.Checked;
            newgame_btn.Visible = radio_multiplayer.Checked;
        }

        private void newgame_btn_Click(object sender, EventArgs e)
        {
            string serverPath = @"..\..\..\Server\bin\Debug\Server.exe";
            FileInfo file = new FileInfo(serverPath);
            if (file.Exists)
            {
                Process.Start(serverPath);
                //Thread.Sleep(3000);

                client = new Client(Game.Username, "127.0.0.1", transmission);

                game = new GameForm();
                game.ConnectServer(client);
                game.Location = new Point(145, 10);
                this.Controls.Add(game);

                groupBox1.Visible = false;
            }
            else
                MessageBox.Show("Server.exe could not found!");
        }

        private void connectgame_btn_Click(object sender, EventArgs e)
        {
            client = new Client(Game.Username, "127.0.0.1", transmission);

            var mb = MessageBox.Show("Connecting..", "Connection", MessageBoxButtons.OK);
            if (mb == DialogResult.OK)
            {
                game = new GameForm(client);
                game.Location = new Point(145, 10);
                this.Controls.Add(game);

                groupBox1.Visible = false;
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }
    }
}
