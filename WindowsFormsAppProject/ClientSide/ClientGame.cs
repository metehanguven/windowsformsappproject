﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppProject.ClientSide
{
    public class ClientGame
    {
        public static string Username;
        private List<List<Flake>> flakes = new List<List<Flake>>();
        private Size boardSize = new Size();
        private int point = 0;
        private int totalPoint = 0;
        private Point startLocation = new Point();
        private Flake selectedFlake;
        private Timer timer = new Timer();
        private Timer commandTimer = new Timer();
        private int commandNumber = 0;
        private int timer_counter = 0;
        private Label label;
        private Client client;
        


        public ClientGame(Client client)
        {
            this.client = client;
        }


        public List<List<Flake>> GetFlakes
        {
            get { return this.flakes; }
        }
        public Point StartLocation
        {
            get { return this.startLocation; }
            set { this.startLocation = value; }
        }
        public int GetTotalPoint
        {
            get { return this.totalPoint; }
        }

        private void LoadFlakes()
        {
            for (int i = 0; i < boardSize.Height; i++)
            {
                List<Flake> horizontal = new List<Flake>();
                for (int j = 0; j < boardSize.Width; j++)
                {
                    var flake = new Flake();
                    flake.Name = "Flake" + i.ToString() + "_" + j.ToString();
                    flake.Click += Flake_Click;
                    flake.WorldLocation = new Point(j, i);
                    flake.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    flake.BackColor = Color.DarkTurquoise;
                    flake.BaseColor = flake.BackColor;
                    flake.Size = new Size(600 / boardSize.Width, 600 / boardSize.Height);
                    flake.Location = new Point(this.startLocation.X + j * (600 / boardSize.Width + 5), this.startLocation.Y + i * (600 / boardSize.Height + 5));
                    horizontal.Add(flake);
                }
                this.flakes.Add(horizontal);
            }
        }

        private void UpdateOptions()
        {
            var transmissions = this.client.GetClientsTransmission.GetTransmission;
            int targetIndex = 0;
            for (int i = 0; i < transmissions.Count; i++)
            {
                if (transmissions[i].IndexOf('%') != -1)
                    targetIndex = i;
            }

            string option = transmissions[targetIndex].Split('%')[1];
            string[] allOptions = option.Split('|');

            //board size
            var board_size = allOptions[0].Split(',');
            this.boardSize = new Size(int.Parse(board_size[0]), int.Parse(board_size[1]));
            this.point = int.Parse(board_size[2]);

            LoadFlakes();

            //Triple flakes update
            var flakes3 = allOptions[1].Split(',');
            for (int i = 0; i < flakes3.Length; i++)
            {
                var tripleInfo = flakes3[i].Split('<');
                //tripleInfo[0] = Coordinate 2-5
                var coorYX = tripleInfo[0].Split('-');
                //coorYX[0] = Y coorYX[1]= X
                //tripleInfo[1] = Shape 0,1,2
                int shapeINT = Convert.ToInt32(tripleInfo[1]);
                Color color = Color.FromArgb(Convert.ToInt32(tripleInfo[2]));
                this.flakes[int.Parse(coorYX[0])][int.Parse(coorYX[1])].BaseColor = color;
                this.flakes[int.Parse(coorYX[0])][int.Parse(coorYX[1])].BackColor = color;
                this.flakes[int.Parse(coorYX[0])][int.Parse(coorYX[1])].Conquered = true;
                this.flakes[int.Parse(coorYX[0])][int.Parse(coorYX[1])].SetShape((Flake.Shapes)shapeINT);
            }
        }

        public void StartGame(Label label)
        {
            timer.Interval = 500;
            timer.Tick += Timer_Tick;

            commandTimer.Interval = 100;
            commandTimer.Tick += CommandTimer_Tick;
            commandTimer.Enabled = true;
            this.label = label;

            UpdateOptions();
        }

        private void Flake_Click(object sender, EventArgs e)
        {
            Flake flake = (Flake)sender;

            if (flake.Conquered)
            {
                if (this.selectedFlake != null && this.selectedFlake.Name != flake.Name)
                {
                    this.selectedFlake.SelectedFlake = false;
                    this.selectedFlake.BackColor = this.selectedFlake.BaseColor;
                    this.client.SendMessage($"Release the flake. %l-{selectedFlake.WorldLocation.X}-{selectedFlake.WorldLocation.Y}");
                }

                this.selectedFlake = flake;

                this.client.SendMessage($"({selectedFlake.WorldLocation.X + 1}-{selectedFlake.WorldLocation.Y + 1})" +
                    $":[{selectedFlake.BaseColor.ToString()},{selectedFlake.GetShape().ToString()}] flake selected." + Environment.NewLine +
                    $"%s-{selectedFlake.WorldLocation.X}-{selectedFlake.WorldLocation.Y}");

                if (!flake.SelectedFlake)
                {
                    flake.BackColor = ControlPaint.LightLight(flake.BaseColor);
                    flake.SelectedFlake = true;
                }
            }
            else
            {
                if (this.selectedFlake != null)
                {
                    this.client.SendMessage($"({selectedFlake.WorldLocation.X + 1}-{selectedFlake.WorldLocation.Y + 1})" +
                            $":[{selectedFlake.BaseColor.ToString()},{selectedFlake.GetShape().ToString()}] moving to" +
                            $" ({flake.WorldLocation.X + 1}-{flake.WorldLocation.Y + 1}) request" + Environment.NewLine +
                            $"%m-{flake.WorldLocation.X}-{flake.WorldLocation.Y}|f-{selectedFlake.WorldLocation.X}-{selectedFlake.WorldLocation.Y}");
                }
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {

        }

        private void CommandTimer_Tick(object sender, EventArgs e)
        {
            var commands = this.client.GetClientsTransmission.GetTransmission;

            if (this.commandNumber != commands.Count && commands.Count > 0
                && commands[commands.Count - 1].IndexOf('%') != -1 && commands[commands.Count - 1].IndexOf('<') == -1)
            {
                string com = commands[commands.Count - 1];
                var cmd1 = com.Split('%')[1];

                switch (cmd1[0])
                {
                    case 's':
                        {
                            var cmds = cmd1.Split('-');
                            int x = Int32.Parse(cmds[1]);
                            int y = int.Parse(cmds[2]);
                            this.flakes[y][x].BackColor = Color.White;
                            break;
                        }
                    case 'l':
                        {
                            var cmds = cmd1.Split('-');
                            int x = int.Parse(cmds[1]); 
                            int y = int.Parse(cmds[2]);
                            this.flakes[y][x].BackColor = this.flakes[y][x].BaseColor;
                            break;
                        }
                    case 'm':
                        {
                            var opts = cmd1.Split('|');
                            var cmds = opts[0].Split('-');
                            int mx = int.Parse(cmds[1]); int my = int.Parse(cmds[2]);
                            var cfds = opts[1].Split('-');
                            int fx = int.Parse(cfds[1]); int fy = int.Parse(cfds[2]);

                            this.flakes[my][mx].BackColor = this.flakes[fy][fx].BackColor;
                            this.flakes[my][mx].SetShape(this.flakes[fy][fx].GetShape());
                            this.flakes[my][mx].BaseColor = this.flakes[fy][fx].BaseColor;
                            this.flakes[my][mx].Conquered = true;
                            this.flakes[fy][fx].BackColor = Color.DarkTurquoise;
                            this.flakes[fy][fx].BaseColor = Color.DarkTurquoise;
                            this.flakes[fy][fx].SetShape(Flake.Shapes.Square);
                            this.flakes[fy][fx].Conquered = false;
                            break;
                        }
                    case 'c':
                        {
                            var elements = cmd1.Split('|');
                            for (int i = 0; i < elements.Length - 1; i++)
                            {
                                var element = elements[i + 1].Split(',');
                                var xy = element[0].Split('-');
                                int x = int.Parse(xy[0]);
                                int y = int.Parse(xy[1]);

                                Flake.Shapes shape = (Flake.Shapes)(int.Parse(element[1]));
                                Color colorCode = Color.FromArgb(int.Parse(element[2]));
                                this.flakes[y][x].BackColor = colorCode;
                                this.flakes[y][x].BaseColor = colorCode;
                                this.flakes[y][x].SetShape(shape);
                                this.flakes[y][x].Conquered = true;
                            }
                            break;
                        }
                    case 'd':
                        {
                            break;
                        }
                    case 'a':
                        {
                            var cmds = cmd1.Split('-');
                            int x = int.Parse(cmds[1]);
                            int y = int.Parse(cmds[2]);
                            this.flakes[y][x].BackColor = this.selectedFlake.BackColor;
                            this.flakes[y][x].SetShape(this.selectedFlake.GetShape());
                            this.flakes[y][x].BaseColor = this.selectedFlake.BaseColor;
                            this.flakes[y][x].SelectedFlake = true;
                            this.flakes[y][x].Conquered = true;
                            this.selectedFlake.BackColor = Color.DarkTurquoise;
                            this.selectedFlake.BaseColor = Color.DarkTurquoise;
                            this.selectedFlake.SetShape(Flake.Shapes.Square);
                            this.selectedFlake.SelectedFlake = false;
                            this.selectedFlake.Conquered = false;
                            this.selectedFlake = this.flakes[y][x];
                            break;
                        }
                }

            }

            this.commandNumber = commands.Count;


        }
    }
}
