﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsAppProject.ClientSide
{
    public class ClientsTransmission
    {
        private List<string> server = new List<string>();

        public ClientsTransmission()
        {

        }

        public void Add(string message)
        {
            this.server.Add(message);
        }

        public List<string> GetTransmission
        {
            get { return this.server; }
        }

    }
}
