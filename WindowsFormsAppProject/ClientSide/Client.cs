﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsFormsAppProject.ClientSide
{
    public class Client
    {
        private string UserName;
        private string host;
        private const int port = 8080;
        private TcpClient client;
        private NetworkStream stream;
        private ClientsTransmission transmission;

        public Client(string username, string host, ClientsTransmission transmission)
        {
            this.transmission = transmission;
            this.host = host;
            this.UserName = username;
            client = new TcpClient();
            try
            {
                client.Connect(IPAddress.Parse(host), port);
                stream = client.GetStream();

                string Message = UserName;
                byte[] buffer = Encoding.Unicode.GetBytes(Message);
                stream.Write(buffer, 0, buffer.Length);

                Thread RecieveThread = new Thread(new ThreadStart(RecieveMessage));
                RecieveThread.Start();

                this.transmission.Add("Welcome, " + UserName);
            }
            catch (Exception ex)
            {
                this.transmission.Add(ex.Message);
            }
            //finally
            //{
            //    Disconnect();
            //}
        }

        public ClientsTransmission GetClientsTransmission
        {
            get { return this.transmission; }
        }
        public void SendMessage(string message)
        {
            byte[] buffer = Encoding.Unicode.GetBytes(message);
            stream.Write(buffer, 0, buffer.Length);
        }

        private void RecieveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] buffer = new byte[64];
                    StringBuilder Builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(buffer, 0, buffer.Length);
                        Builder.Append(Encoding.Unicode.GetString(buffer, 0, bytes));
                    } while (stream.DataAvailable);

                    string Message = Builder.ToString();
                    this.transmission.Add(Message);
                }
                catch
                {
                    Console.WriteLine("Disconnected!");
                    Disconnect();
                }
            }
        }

        public void Disconnect()
        {
            if (stream != null)
            {
                stream.Close();
            }
            if (client != null)
            {
                client.Close();
            }
        }

    }
}
