﻿namespace WindowsFormsAppProject
{
    partial class Dif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dif));
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.btnap = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtn1 = new System.Windows.Forms.TextBox();
            this.txtn2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblsum = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.imageList4 = new System.Windows.Forms.ImageList(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.chcblue = new System.Windows.Forms.CheckBox();
            this.chcred = new System.Windows.Forms.CheckBox();
            this.chcyellow = new System.Windows.Forms.CheckBox();
            this.chcpurple = new System.Windows.Forms.CheckBox();
            this.chcgreen = new System.Windows.Forms.CheckBox();
            this.chcorange = new System.Windows.Forms.CheckBox();
            this.chcüç = new System.Windows.Forms.CheckBox();
            this.chcyuv = new System.Windows.Forms.CheckBox();
            this.chckare = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(518, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 52);
            this.label1.TabIndex = 0;
            this.label1.Text = "SETTINGS";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioButton1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButton1.Location = new System.Drawing.Point(780, 159);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(81, 30);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.Text = "EASY";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioButton2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButton2.Location = new System.Drawing.Point(985, 159);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(122, 30);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.Text = "NORMAL";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radioButton3.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioButton3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButton3.Location = new System.Drawing.Point(780, 261);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(91, 30);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.Text = "HARD";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.radioButton4.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radioButton4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButton4.Location = new System.Drawing.Point(985, 261);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(120, 30);
            this.radioButton4.TabIndex = 5;
            this.radioButton4.Text = "CUSTOM";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // btnap
            // 
            this.btnap.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnap.ForeColor = System.Drawing.Color.Black;
            this.btnap.Location = new System.Drawing.Point(558, 583);
            this.btnap.Name = "btnap";
            this.btnap.Size = new System.Drawing.Size(100, 35);
            this.btnap.TabIndex = 6;
            this.btnap.Text = "APPLY";
            this.btnap.UseVisualStyleBackColor = true;
            this.btnap.Click += new System.EventHandler(this.btnap_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.ImageKey = "back_button.png";
            this.button1.ImageList = this.imageList1;
            this.button1.Location = new System.Drawing.Point(31, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 92);
            this.button1.TabIndex = 7;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "back_button.png");
            // 
            // txtn1
            // 
            this.txtn1.Location = new System.Drawing.Point(985, 311);
            this.txtn1.Name = "txtn1";
            this.txtn1.Size = new System.Drawing.Size(37, 22);
            this.txtn1.TabIndex = 8;
            this.txtn1.Visible = false;
            this.txtn1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtn1_KeyPress);
            // 
            // txtn2
            // 
            this.txtn2.Location = new System.Drawing.Point(1068, 313);
            this.txtn2.Name = "txtn2";
            this.txtn2.Size = new System.Drawing.Size(37, 22);
            this.txtn2.TabIndex = 9;
            this.txtn2.Visible = false;
            this.txtn2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtn2_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1037, 316);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "+";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1138, 316);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "=";
            this.label3.Visible = false;
            // 
            // lblsum
            // 
            this.lblsum.AutoSize = true;
            this.lblsum.Location = new System.Drawing.Point(1171, 318);
            this.lblsum.Name = "lblsum";
            this.lblsum.Size = new System.Drawing.Size(0, 17);
            this.lblsum.TabIndex = 13;
            this.lblsum.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1005, 352);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 29);
            this.button2.TabIndex = 14;
            this.button2.Text = "Sum Up";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(849, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "DIFFICULTY";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(191, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 31);
            this.label5.TabIndex = 15;
            this.label5.Text = "SHAPES";
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "indir.png");
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "indir (1).jpg");
            // 
            // imageList4
            // 
            this.imageList4.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4.ImageStream")));
            this.imageList4.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList4.Images.SetKeyName(0, "indir.jpg");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(849, 404);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 31);
            this.label6.TabIndex = 17;
            this.label6.Text = "COLOUR";
            // 
            // chcblue
            // 
            this.chcblue.AutoSize = true;
            this.chcblue.Location = new System.Drawing.Point(780, 466);
            this.chcblue.Name = "chcblue";
            this.chcblue.Size = new System.Drawing.Size(58, 21);
            this.chcblue.TabIndex = 18;
            this.chcblue.Text = "Blue";
            this.chcblue.UseVisualStyleBackColor = true;
            this.chcblue.CheckedChanged += new System.EventHandler(this.chcblue_CheckedChanged);
            // 
            // chcred
            // 
            this.chcred.AutoSize = true;
            this.chcred.Location = new System.Drawing.Point(780, 494);
            this.chcred.Name = "chcred";
            this.chcred.Size = new System.Drawing.Size(56, 21);
            this.chcred.TabIndex = 19;
            this.chcred.Text = "Red";
            this.chcred.UseVisualStyleBackColor = true;
            this.chcred.CheckedChanged += new System.EventHandler(this.chcred_CheckedChanged);
            // 
            // chcyellow
            // 
            this.chcyellow.AutoSize = true;
            this.chcyellow.Location = new System.Drawing.Point(780, 522);
            this.chcyellow.Name = "chcyellow";
            this.chcyellow.Size = new System.Drawing.Size(70, 21);
            this.chcyellow.TabIndex = 20;
            this.chcyellow.Text = "Yellow";
            this.chcyellow.UseVisualStyleBackColor = true;
            this.chcyellow.CheckedChanged += new System.EventHandler(this.chcyellow_CheckedChanged);
            // 
            // chcpurple
            // 
            this.chcpurple.AutoSize = true;
            this.chcpurple.Location = new System.Drawing.Point(969, 466);
            this.chcpurple.Name = "chcpurple";
            this.chcpurple.Size = new System.Drawing.Size(71, 21);
            this.chcpurple.TabIndex = 21;
            this.chcpurple.Text = "Purple";
            this.chcpurple.UseVisualStyleBackColor = true;
            this.chcpurple.CheckedChanged += new System.EventHandler(this.chcpurple_CheckedChanged);
            // 
            // chcgreen
            // 
            this.chcgreen.AutoSize = true;
            this.chcgreen.Location = new System.Drawing.Point(969, 493);
            this.chcgreen.Name = "chcgreen";
            this.chcgreen.Size = new System.Drawing.Size(70, 21);
            this.chcgreen.TabIndex = 22;
            this.chcgreen.Text = "Green";
            this.chcgreen.UseVisualStyleBackColor = true;
            this.chcgreen.CheckedChanged += new System.EventHandler(this.chcgreen_CheckedChanged);
            // 
            // chcorange
            // 
            this.chcorange.AutoSize = true;
            this.chcorange.Location = new System.Drawing.Point(969, 520);
            this.chcorange.Name = "chcorange";
            this.chcorange.Size = new System.Drawing.Size(78, 21);
            this.chcorange.TabIndex = 23;
            this.chcorange.Text = "Orange";
            this.chcorange.UseVisualStyleBackColor = true;
            this.chcorange.CheckedChanged += new System.EventHandler(this.chcorange_CheckedChanged);
            // 
            // chcüç
            // 
            this.chcüç.AutoSize = true;
            this.chcüç.ImageKey = "indir.png";
            this.chcüç.ImageList = this.imageList2;
            this.chcüç.Location = new System.Drawing.Point(41, 167);
            this.chcüç.Name = "chcüç";
            this.chcüç.Size = new System.Drawing.Size(148, 130);
            this.chcüç.TabIndex = 24;
            this.chcüç.UseVisualStyleBackColor = true;
            // 
            // chcyuv
            // 
            this.chcyuv.AutoSize = true;
            this.chcyuv.ImageKey = "indir (1).jpg";
            this.chcyuv.ImageList = this.imageList3;
            this.chcyuv.Location = new System.Drawing.Point(359, 167);
            this.chcyuv.Name = "chcyuv";
            this.chcyuv.Size = new System.Drawing.Size(143, 125);
            this.chcyuv.TabIndex = 25;
            this.chcyuv.UseVisualStyleBackColor = true;
            // 
            // chckare
            // 
            this.chckare.AutoSize = true;
            this.chckare.ImageKey = "indir.jpg";
            this.chckare.ImageList = this.imageList4;
            this.chckare.Location = new System.Drawing.Point(180, 359);
            this.chckare.Name = "chckare";
            this.chckare.Size = new System.Drawing.Size(143, 150);
            this.chckare.TabIndex = 26;
            this.chckare.UseVisualStyleBackColor = true;
            // 
            // Dif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1212, 642);
            this.Controls.Add(this.chckare);
            this.Controls.Add(this.chcyuv);
            this.Controls.Add(this.chcüç);
            this.Controls.Add(this.chcorange);
            this.Controls.Add(this.chcgreen);
            this.Controls.Add(this.chcpurple);
            this.Controls.Add(this.chcyellow);
            this.Controls.Add(this.chcred);
            this.Controls.Add(this.chcblue);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblsum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtn2);
            this.Controls.Add(this.txtn1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnap);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label1);
            this.Name = "Dif";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Dif_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Button btnap;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TextBox txtn1;
        private System.Windows.Forms.TextBox txtn2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblsum;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ImageList imageList4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chcblue;
        private System.Windows.Forms.CheckBox chcred;
        private System.Windows.Forms.CheckBox chcyellow;
        private System.Windows.Forms.CheckBox chcpurple;
        private System.Windows.Forms.CheckBox chcgreen;
        private System.Windows.Forms.CheckBox chcorange;
        private System.Windows.Forms.CheckBox chcüç;
        private System.Windows.Forms.CheckBox chcyuv;
        private System.Windows.Forms.CheckBox chckare;
    }
}