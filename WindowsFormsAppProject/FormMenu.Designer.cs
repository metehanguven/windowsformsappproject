﻿namespace WindowsFormsAppProject
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.btnstart = new System.Windows.Forms.Button();
            this.btnex = new System.Windows.Forms.Button();
            this.btnset = new System.Windows.Forms.Button();
            this.btnpro = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.connectgame_btn = new System.Windows.Forms.Button();
            this.newgame_btn = new System.Windows.Forms.Button();
            this.radio_multiplayer = new System.Windows.Forms.RadioButton();
            this.radio_single = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnstart
            // 
            this.btnstart.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnstart.Location = new System.Drawing.Point(12, 207);
            this.btnstart.Margin = new System.Windows.Forms.Padding(2);
            this.btnstart.Name = "btnstart";
            this.btnstart.Size = new System.Drawing.Size(112, 50);
            this.btnstart.TabIndex = 0;
            this.btnstart.Text = "START";
            this.btnstart.UseVisualStyleBackColor = true;
            this.btnstart.Click += new System.EventHandler(this.btnstart_Click);
            // 
            // btnex
            // 
            this.btnex.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnex.Location = new System.Drawing.Point(12, 403);
            this.btnex.Margin = new System.Windows.Forms.Padding(2);
            this.btnex.Name = "btnex";
            this.btnex.Size = new System.Drawing.Size(112, 50);
            this.btnex.TabIndex = 1;
            this.btnex.Text = "EXIT";
            this.btnex.UseVisualStyleBackColor = true;
            this.btnex.Click += new System.EventHandler(this.btnex_Click);
            // 
            // btnset
            // 
            this.btnset.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnset.Location = new System.Drawing.Point(12, 338);
            this.btnset.Margin = new System.Windows.Forms.Padding(2);
            this.btnset.Name = "btnset";
            this.btnset.Size = new System.Drawing.Size(112, 50);
            this.btnset.TabIndex = 3;
            this.btnset.Text = "SETTINGS";
            this.btnset.UseVisualStyleBackColor = true;
            this.btnset.Click += new System.EventHandler(this.btnset_Click);
            // 
            // btnpro
            // 
            this.btnpro.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnpro.Location = new System.Drawing.Point(12, 271);
            this.btnpro.Margin = new System.Windows.Forms.Padding(2);
            this.btnpro.Name = "btnpro";
            this.btnpro.Size = new System.Drawing.Size(112, 50);
            this.btnpro.TabIndex = 4;
            this.btnpro.Text = "PROFILE";
            this.btnpro.UseVisualStyleBackColor = true;
            this.btnpro.Click += new System.EventHandler(this.btnpro_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.connectgame_btn);
            this.groupBox1.Controls.Add(this.newgame_btn);
            this.groupBox1.Controls.Add(this.radio_multiplayer);
            this.groupBox1.Controls.Add(this.radio_single);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.ForeColor = System.Drawing.Color.Coral;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(111, 190);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Play";
            // 
            // connectgame_btn
            // 
            this.connectgame_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.connectgame_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.connectgame_btn.ForeColor = System.Drawing.Color.LawnGreen;
            this.connectgame_btn.Location = new System.Drawing.Point(6, 137);
            this.connectgame_btn.Name = "connectgame_btn";
            this.connectgame_btn.Size = new System.Drawing.Size(99, 47);
            this.connectgame_btn.TabIndex = 11;
            this.connectgame_btn.Text = "Connect Game";
            this.connectgame_btn.UseVisualStyleBackColor = true;
            this.connectgame_btn.Visible = false;
            this.connectgame_btn.Click += new System.EventHandler(this.connectgame_btn_Click);
            // 
            // newgame_btn
            // 
            this.newgame_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newgame_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.newgame_btn.Location = new System.Drawing.Point(6, 86);
            this.newgame_btn.Name = "newgame_btn";
            this.newgame_btn.Size = new System.Drawing.Size(99, 47);
            this.newgame_btn.TabIndex = 9;
            this.newgame_btn.Text = "New Game";
            this.newgame_btn.UseVisualStyleBackColor = true;
            this.newgame_btn.Visible = false;
            this.newgame_btn.Click += new System.EventHandler(this.newgame_btn_Click);
            // 
            // radio_multiplayer
            // 
            this.radio_multiplayer.AutoSize = true;
            this.radio_multiplayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radio_multiplayer.ForeColor = System.Drawing.Color.White;
            this.radio_multiplayer.Location = new System.Drawing.Point(6, 56);
            this.radio_multiplayer.Name = "radio_multiplayer";
            this.radio_multiplayer.Size = new System.Drawing.Size(96, 22);
            this.radio_multiplayer.TabIndex = 10;
            this.radio_multiplayer.Text = "Multiplayer";
            this.radio_multiplayer.UseVisualStyleBackColor = true;
            this.radio_multiplayer.CheckedChanged += new System.EventHandler(this.radio_multiplayer_CheckedChanged);
            // 
            // radio_single
            // 
            this.radio_single.AutoSize = true;
            this.radio_single.Checked = true;
            this.radio_single.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.radio_single.ForeColor = System.Drawing.Color.White;
            this.radio_single.Location = new System.Drawing.Point(6, 28);
            this.radio_single.Name = "radio_single";
            this.radio_single.Size = new System.Drawing.Size(66, 22);
            this.radio_single.TabIndex = 9;
            this.radio_single.TabStop = true;
            this.radio_single.Text = "Single";
            this.radio_single.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(36)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(1034, 682);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnpro);
            this.Controls.Add(this.btnset);
            this.Controls.Add(this.btnex);
            this.Controls.Add(this.btnstart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnstart;
        private System.Windows.Forms.Button btnex;
        private System.Windows.Forms.Button btnset;
        private System.Windows.Forms.Button btnpro;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button connectgame_btn;
        private System.Windows.Forms.Button newgame_btn;
        private System.Windows.Forms.RadioButton radio_multiplayer;
        private System.Windows.Forms.RadioButton radio_single;
    }
}