﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsAppProject
{
    public static class ImageProvider
    {
        public enum ImageNicks
        {
            Default = 0,
            User = 1,
            Admin = 2,
            Login_btn_1 = 3,
            Login_btn_2 = 4,
            Logout = 5,
            Triangle = 6,
            Circle = 7,
            Square = 8,
        }
        public static Image ImageProvide(ImageNicks nick)
        {
            switch (nick)
            {
                case ImageNicks.Default:
                    return WindowsFormsAppProject.Properties.Resources._default;
                case ImageNicks.User:
                    return GetTryImage("ph_admin.png");
                case ImageNicks.Admin:
                    return GetTryImage("ph_user.png");
                case ImageNicks.Login_btn_1:
                    return GetTryImage("login_btn_1.png");
                case ImageNicks.Login_btn_2:
                    return GetTryImage("login_btn_2.png");
                case ImageNicks.Logout:
                    return GetTryImage("logout.png");
                case ImageNicks.Circle:
                    return GetTryImage("circle.png");
                case ImageNicks.Square:
                    return GetTryImage("square.png");
                case ImageNicks.Triangle:
                    return GetTryImage("triangle.png");
                default:
                    return WindowsFormsAppProject.Properties.Resources._default;
            }
        }

        public static Image GetTryImage(string filename)
        {
            FileInfo info = new FileInfo("..\\..\\img\\" + filename);
            if (info.Exists)
                return Image.FromFile("..\\..\\img\\" + filename);
            else
                return WindowsFormsAppProject.Properties.Resources._default;
        }

    }
}
