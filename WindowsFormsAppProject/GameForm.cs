﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsAppProject.ClientSide;

namespace WindowsFormsAppProject
{
    public partial class GameForm : UserControl
    {
        Game game;
        ClientGame clientGame;
        bool isClient = false;
        public GameForm()
        {
            InitializeComponent();
            game = new Game();
        }

        public GameForm(Client client)
        {
            InitializeComponent();
            isClient = true;
            this.clientGame = new ClientGame(client);
        }

        public string GetOptions
        {
            get { return game.GetOptions(); }
        }

        public void ConnectServer(Client client)
        {
            game.ConnectServer(client);
        }
        private void GameForm_Load(object sender, EventArgs e)
        {
            if (!isClient) //Server and single game.
            {
                game.StartGame(lblPoint);
                var flakes = game.GetFlakes;
                for (int i = 0; i < flakes.Count; i++)
                {
                    this.Controls.AddRange(flakes[i].ToArray());
                }
            }
            else //Clientgame
            {
                clientGame.StartGame(lblPoint);
                var flakes = clientGame.GetFlakes;
                for (int i = 0; i < flakes.Count; i++)
                {
                    this.Controls.AddRange(flakes[i].ToArray());
                }
            }
        }

        
    }
}
