﻿CREATE DATABASE OOP_ProjectDB
GO
USE OOP_ProjectDB
GO
CREATE TABLE Bilgi(
	UserID			int				IDENTITY (1, 1),
	Username		varchar(32)		NOT NULL,
	Password		varchar(32)		NOT NULL,
	Name_Surname	varchar(64)		NOT NULL,
	Phone_Number	varchar(16)		NOT NULL,
	Address			varchar(256),
	City			varchar(64),
	Country			varchar(64),
	E_mail			varchar(256)	NOT NULL,

	CONSTRAINT	PK_Bilgi_UserID				PRIMARY KEY(UserID),
	CONSTRAINT	UQ_Bilgi_Username			UNIQUE (Username),
	CONSTRAINT	UQ_Bilgi_E_mail				UNIQUE (E_mail)
);
GO
CREATE INDEX IN_Bilgi_Username_Email	ON Bilgi(Username, E_mail);
GO